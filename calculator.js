
let storeVal = 0;

let tempVal = [];
let tempValConv = 0;

let operandUsed = [false,false,false,false];
let isAnswerDisplayed = false;

// * * * * * * * * * * * * * * * * * * * * * * * *

function clickNumberButton(num){
    console.log(num);
    storeNumber(num);

    isAnswerDisplayed = false;
}

function clickOperatorButton(operator){
    console.log(operator + "が押されました！");

    tempValConv = convertTempVal();

    // checkOperandはやはり、先に行う必要がある
    checkOperand();

    if (operator==='+') {
        operandUsed[0] = true;
        console.log('operandUsed[0]:',operandUsed[0]);
    } else if (operator==='-') {
        operandUsed[1] = true;
        console.log('operandUsed[1]:',operandUsed[1]);
    } else if (operator==='*') {
        operandUsed[2] = true;
        console.log('operandUsed[2]:',operandUsed[2]);
    } else if (operator==='/') {
        operandUsed[3] = true;
        console.log('operandUsed[3]:',operandUsed[3]);
    }
    // =とdelの処理はここでは行わなくて良い
    // 演算子を記憶する様のbooleanの配列を作成して、ここでtrueにセットする管理をする
    
    // 演算子を読み取って処理を行なった後、配列tempValの値はリセットする
    resetTempVal();
    isAnswerDisplayed = false;
}

// * * * * * *
function checkOperand () {
    // ここの中で、演算子が既に押されているかどうかを確認して計算の処理を行う
    if (operandUsed[0]) {
        storeVal += convertTempVal();
        operandUsed[0] = false;
    } else if (operandUsed[1]) {
        storeVal -= convertTempVal();
        operandUsed[1] = false;
    } else if (operandUsed[2]) {
        storeVal *= convertTempVal();
        operandUsed[2] = false;
    } else if (operandUsed[3]) {
        storeVal /= convertTempVal();
        operandUsed[3] = false;
    } else {
        if (!isAnswerDisplayed) {
            storeVal = convertTempVal();
        }
    }
    //} else {
    //    console.log('no operand used');
    //}

    // 演算子を記憶する様のbooleanの配列をfalseにセットし直す

}


// * * * * * *


//function clickClearButton(){
//    alert("クリアボタンが押されました！");
//}

function clickEqualButton(){
    console.log("イコールボタンが押されました！");

    checkOperand('='); // 後々加える必要がある
    isAnswerDisplayed = true;

    // ここで演算子が既に入力されているかどうかなどの条件分けを行う必要があるが、
    // 今は、イコールが一度押された後の処理のことは考えなくて良い

    let displayAnwer = storeVal;
    console.log('ANSWER',displayAnwer);
    alert('ANSWER: '+displayAnwer);
}

function clickDeleteButton(){
    alert("デリートボタンが押されました！");
    if (isAnswerDisplayed){
        resetTempVal();
    } else {
        deleteTempValByElement();
    }
    

}


// * * * * * * * * * * * * * * * * * * * * * * * *

// store iput values as an array
function storeNumber (val) {
    //val = String (val);
    if(isAnswerDisplayed) {
        resetTempVal();
    }
    tempVal.push(val);
}

// convert the input values to one variable
function convertTempVal () {
    tempValConv = 0;
    
    for (let j=1,i=tempVal.length-1;i>=0;i--) {
        tempValConv += tempVal[i] * j;
        j *= 10;
    }
    return tempValConv;
}

// delete en element from an array of input values
function deleteTempValByElement () {
    tempVal.splice(tempVal.length-1,1);
}

// delete and reset tempVal array 
function resetTempVal () {
    for (let i=tempVal.length-1;i>=0;i--) {
        tempVal.splice(i,1);
    }

}